// EXPRESS SETUP
// 1. Import by using the 'require' directive to get access to the components of express package or dependency
const express = require('express')
// 2. Use the express() method/function and assign it to an app variable to create an express app or app server
const app = express()
// 3. Declare a variable for the port of the server
const port = 3000

// Middlewares
// These two .use are essential in express
// Allows your app tto read json format data
app.use(express.json())
// Allows your app to read data from forms.
app.use(express.urlencoded({extended: true}))

// Routes

// Get request route
app.get('/', (request, response) => {
	// once the route is accessed it will tehn send a string response containing "Hello World"
	response.send('Hello World')
})
// This route expects to receive a GET request at the URI "/hello"
app.get('/hello', (request, response) => {
	response.send('Hello from /hello endpoint!')
})

// Register user route

// An array that will store user objects/documents when the "/register" route is accessed
// This will also serve as our mock database
let users = [];

// This route expectes to receive a POST request at the URI "/register"
// This will create a suer object in the "users" variable that mirrors a real world registration process
app.post('/register', (request, response) => {
	if(request.body.username !== " " && request.body.password !== " ")
		{users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} successfully registered`)} 
	else {response.send('Please input BOTH username and password')}
})

app.put('/change-password', (request, response) => {
	let message

	for(let i = 0; i < users.length; i ++) {
		if(request.body.username == users[i].username) {
			users[i].password == request.body.password
			message = `User ${request.body.username}'s password has been updated!`
			break}
		else {message = 'User does not exist.'}
	}
	response.send(message)
})
// Activity

// 1. GET /home
app.get('/home', (request, response) => {
	response.send('Welcome to the home page!')
})

// 2. GET /users
app.get('/users', (request, response) => {
	response.send(users)
})

// 3. Delete
app.delete('/delete-user', (request,response) => {
    let message

	for(let i = 0; i < users.length; i++) {
		if(request.body.username == users[i].username){
			users.splice(i,1);
			message = `User ${request.body.username} has been deleted!`
			break
		} else {
			message = 'User does not exist.'
		}
	}
	response.send(message)
})

 

app.listen(port, () => console.log(`Server is running at port ${port}`));

